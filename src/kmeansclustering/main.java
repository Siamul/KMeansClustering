/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeansclustering;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Siamul Karim Khan
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String file = "bisecting.txt";
        String rline;
        ArrayList<String[]> dataList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            while((rline = br.readLine()) != null)
            {
                dataList.add(rline.split("	"));
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        String[][] dataSetStr = new String[dataList.size()][];
        dataSetStr = dataList.toArray(dataSetStr);
        double[][] dataSet = new double[dataSetStr.length][];
        for(int i = 0; i<dataSetStr.length; i++)
        {
            dataSet[i] = new double[dataSetStr[i].length];
            for(int j = 0; j<dataSetStr[i].length; j++)
            {
                dataSet[i][j] = Double.parseDouble(dataSetStr[i][j]);
            }
        }
//        for(int i = 0; i < dataSet.length; i++)
//        {
//            for(int j = 0; j < dataSet[i].length; j++)
//            {
//                System.out.print(dataSet[i][j] + " ");
//            }
//            System.out.println();
//        }
//        ArrayList<Integer> initCluster = new ArrayList<>();
//        for(int i = 0; i<dataSet.length; i++)
//        {
//            initCluster.add(i);
//        }
//        Bisector bisection = new Bisector(dataSet, initCluster);
//        bisection.printClusters();
        BisectingKMeans init = new BisectingKMeans(dataSet, 15, 20); //2nd argument is cluster number, change here
        ArrayList<Integer>[] initClusters = init.getClusters();
        double[][] initClusterMeans = init.getClusterMeans();
        for(int i = 0; i<initClusters.length; i++)
        {
            System.out.println(initClusters[i]);
        }
        BasicKMeans clusterer = new BasicKMeans(dataSet, initClusters, initClusterMeans);
        ArrayList<Integer>[] finalClusters = clusterer.getClusters();
        double[][] finalClusterMeans = clusterer.getClusterMeans();
        System.out.println("=========================================================================================================================");
        for(int i = 0; i<finalClusters.length; i++)
        {
            System.out.println(finalClusters[i]);
        }
        Graph g = new Graph();
        g.set(dataSet, finalClusters, finalClusterMeans);
        g.setVisible(true);
    }
    
}
