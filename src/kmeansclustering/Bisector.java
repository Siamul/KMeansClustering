/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeansclustering;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Siamul Karim Khan
 */
public class Bisector {
    private double[] meanVector1;
    private double[] meanVector2;
    private ArrayList<Integer> cluster1 = new ArrayList<>();
    private ArrayList<Integer> cluster2 = new ArrayList<>();

    public double[] getMeanVector1() {
        return meanVector1;
    }

    public double[] getMeanVector2() {
        return meanVector2;
    }

    public ArrayList<Integer> getCluster1() {
        return cluster1;
    }

    public ArrayList<Integer> getCluster2() {
        return cluster2;
    }
    
    public Bisector(double[][] dataSet, ArrayList<Integer> cluster)
    {
        double maxDist = Double.NEGATIVE_INFINITY;
        int skip1 = 0, skip2 = 0;
//        for(int i = 0; i<cluster.size(); i++)
//        {
//            for(int j = i+1; j<cluster.size(); j++)
//            {
//                double dist = distanceBetween(dataSet[cluster.get(i)], dataSet[cluster.get(j)]);
//                if(dist > maxDist)
//                {
//                    maxDist = dist;
//                    meanVector1 = dataSet[cluster.get(i)];
//                    meanVector2 = dataSet[cluster.get(j)];
//                    skip1 = i;
//                    skip2 = j;
//                }
//            }
//        }
        ArrayList<Integer> rand = new ArrayList<>();
        for(int i = 0; i<cluster.size(); i++)
        {
            rand.add(i);
        }
        Collections.shuffle(rand);
        skip1 = rand.get(0);
        skip2 = rand.get(1);
        meanVector1 = dataSet[cluster.get(skip1)];
        meanVector2 = dataSet[cluster.get(skip2)];
        cluster1.add(cluster.get(skip1));
        cluster2.add(cluster.get(skip2));
        for(int i = 0; i<cluster.size(); i++)
        {
            if(i != skip1 && i != skip2)
            {
                double distance1 = distanceBetween(dataSet[cluster.get(i)], meanVector1);
                double distance2 = distanceBetween(dataSet[cluster.get(i)], meanVector2);
                if(distance1 <= distance2)
                {
                    meanVector1 = getMeanVectorAfterAdd(meanVector1, dataSet[cluster.get(i)], cluster1.size());
                    cluster1.add(cluster.get(i));
                }
                else 
                {
                    meanVector2 = getMeanVectorAfterAdd(meanVector2, dataSet[cluster.get(i)], cluster2.size());
                    cluster2.add(cluster.get(i));
                }
            }
        }
        int swap;
        do{
            swap = 0;
            for(int i = 0; i<cluster.size(); i++)
            {
                double distance1 = distanceBetween(dataSet[cluster.get(i)], meanVector1);
                double distance2 = distanceBetween(dataSet[cluster.get(i)], meanVector2);
                if(distance1 < distance2)
                {
                    if(cluster2.contains(cluster.get(i)))
                    {
                        meanVector2 = getMeanVectorAfterSub(meanVector2, dataSet[cluster.get(i)], cluster2.size());
                        meanVector1 = getMeanVectorAfterAdd(meanVector1, dataSet[cluster.get(i)], cluster1.size());
                        cluster2.remove(cluster.get(i));
                        cluster1.add(cluster.get(i));
                        swap++;
                    }
                }
                else if(distance1 > distance2)
                {
                    if(cluster1.contains(cluster.get(i)))
                    {
                        meanVector1 = getMeanVectorAfterSub(meanVector1, dataSet[cluster.get(i)], cluster1.size());
                        meanVector2 = getMeanVectorAfterAdd(meanVector2, dataSet[cluster.get(i)], cluster2.size());
                        cluster1.remove(cluster.get(i));
                        cluster2.add(cluster.get(i));
                        swap++;
                    }
                }
            }
        }while(swap!=0);
    }
    private double distanceBetween(double[] point1, double[] point2)
    {
        double result = 0;
        for(int i = 0; i<point1.length; i++)
        {
            result += Math.pow((point1[i] - point2[i]),2);
        }
        return result;
    }
    private double[] getMeanVectorAfterAdd(double[] currentMean, double[] newPoint, int size)
    {
        double[] retV = new double[currentMean.length];
        for(int i = 0; i<retV.length; i++)
        {
            retV[i] = currentMean[i] * size + newPoint[i];
            retV[i] /= (size + 1);
        }
        return retV;
    }
    private double[] getMeanVectorAfterSub(double[] currentMean, double[] pointRemoved, int size)
    {
        double[] retV = new double[currentMean.length];
        for(int i = 0; i<retV.length; i++)
        {
            retV[i] = currentMean[i] * size - pointRemoved[i];
            retV[i] /= (size - 1);
        }
        return retV;
    }
    public void printClusters()
    {
        System.out.println(cluster1);
        System.out.println(cluster2);
    }
}
