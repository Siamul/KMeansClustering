/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeansclustering;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Siamul Karim Khan
 */
public class BisectingKMeans {
    private int k;

    public int getK() {
        return k;
    }

    public ArrayList<Integer>[] getClusters() {
        return clusters;
    }

    public double[][] getClusterMeans() {
        return clusterMeans;
    }
    private int clusterIndex = 0;
    private ArrayList<Integer>[] clusters;
    private double[][] clusterMeans;
    public BisectingKMeans(double[][] dataSet, int k, int iter)
    {
        this.k = k;
        clusters = new ArrayList[k];
        clusterMeans = new double[k][];
        ArrayList<Integer> initCluster = new ArrayList<>();
        for(int i = 0; i<dataSet.length; i++)
        {
            initCluster.add(i);
        }
        ArrayList<Integer>[][] clusterList = new ArrayList[iter][dataSet[0].length];
        double[][][] clusterListMeans = new double[iter][2][];
        double[] clusterListSSE = new double[iter];
        for(int i = 0; i<iter; i++)
        {
            Collections.shuffle(initCluster);
            Bisector bisect = new Bisector(dataSet, initCluster);
            clusterList[i][0] = bisect.getCluster1();
            clusterListMeans[i][0] = bisect.getMeanVector1();
            clusterList[i][1] = bisect.getCluster2();
            clusterListMeans[i][1] = bisect.getMeanVector2();
            clusterListSSE[i] = singleClusterSSE(dataSet, clusterList[i][0], clusterListMeans[i][0]) + 
                    singleClusterSSE(dataSet, clusterList[i][1], clusterListMeans[i][1]);
        }
        int minIndex = getMinIndex(clusterListSSE);
        clusters[clusterIndex] = clusterList[minIndex][0];
        clusterMeans[clusterIndex++] = clusterListMeans[minIndex][0];
        clusters[clusterIndex] = clusterList[minIndex][1];
        clusterMeans[clusterIndex++] = clusterListMeans[minIndex][1];
        while(clusterIndex != k)
        {
            double[] clusterSizes = new double[clusterIndex];
            for(int i = 0; i<clusterIndex; i++)
            {
                clusterSizes[i] = singleClusterSSE(dataSet, clusters[i], clusterMeans[i]);
            }
            int selectedCluster = getMaxIndex(clusterSizes);
            for(int i = 0; i<iter; i++)
            {
                Collections.shuffle(clusters[selectedCluster]);
                Bisector bisect = new Bisector(dataSet, clusters[selectedCluster]);
                clusterList[i][0] = bisect.getCluster1();
                clusterListMeans[i][0] = bisect.getMeanVector1();
                clusterList[i][1] = bisect.getCluster2();
                clusterListMeans[i][1] = bisect.getMeanVector2();
                clusterListSSE[i] = singleClusterSSE(dataSet, clusterList[i][0], clusterListMeans[i][0]) + 
                        singleClusterSSE(dataSet, clusterList[i][1], clusterListMeans[i][1]);
            }
            minIndex = getMinIndex(clusterListSSE);
            clusters[selectedCluster] = clusterList[minIndex][0];
            clusterMeans[selectedCluster] = clusterListMeans[minIndex][0];
            clusters[clusterIndex] = clusterList[minIndex][1];
            clusterMeans[clusterIndex++] = clusterListMeans[minIndex][1];
        }
        
    }
    
    private int getMinIndex(double[] list)
    {
        int minIndex = -1;
        double min = Double.POSITIVE_INFINITY;
        for(int i = 0; i<list.length; i++)
        {
            if(list[i] < min)
            {
                min = list[i];
                minIndex = i;
            }
        }
        return minIndex;
    }
    
    private int getMaxIndex(double[] list)
    {
        int maxIndex = -1;
        double max = Double.NEGATIVE_INFINITY;
        for(int i = 0; i<list.length; i++)
        {
            if(list[i] > max)
            {
                max = list[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }
    
    private double singleClusterSSE(double[][] dataSet, ArrayList<Integer> cluster, double[] clusterMean)
    {
        double error = 0;
        for(int i = 0; i<cluster.size(); i++)
        {
            error += squaredError(dataSet[cluster.get(i)], clusterMean);
        }
        return error;
    }
    
    private double squaredError(double[] point1, double[] point2)
    {
        double result = 0;
        for(int i = 0; i<point1.length; i++)
        {
            result += Math.pow((point1[i] - point2[i]),2);
        }
        return result;
    }
}
