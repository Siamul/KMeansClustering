/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeansclustering;

import java.util.ArrayList;

/**
 *
 * @author Siamul Karim Khan
 */
public class BasicKMeans {
    private ArrayList<Integer>[] clusters;
    private double[][] clusterMeans;

    public ArrayList<Integer>[] getClusters() {
        return clusters;
    }

    public double[][] getClusterMeans() {
        return clusterMeans;
    }
    public BasicKMeans(double[][] dataSet, ArrayList<Integer>[] initClusters, double[][] initMeans)
    {
        clusters = new ArrayList[initClusters.length];
        clusterMeans = new double[initMeans.length][];
        System.arraycopy(initClusters, 0, clusters, 0, initClusters.length);
        System.arraycopy(initMeans, 0, clusterMeans, 0, initMeans.length);
        double[] distances = new double[clusters.length];
        int swap;
        do{
            swap = 0;
            for(int i = 0; i<clusters.length; i++)
            {
                for(Integer j : clusters[i])
                {
                    for(int k = 0; k<clusters.length; k++)
                    {
                        distances[k] = distanceBetween(dataSet[j], clusterMeans[k]);
                    }
                    int minIndex = getMinIndex(distances);
                    if(minIndex != i)
                    {
                        clusterMeans[minIndex] = 
                                getMeanVectorAfterAdd(clusterMeans[minIndex], 
                                        dataSet[j], clusters[minIndex].size());
                        clusters[minIndex].add(j);
                        clusterMeans[i] = 
                                getMeanVectorAfterSub(clusterMeans[i], 
                                        dataSet[j], clusters[minIndex].size());
                        clusters[i].remove(j);
                        swap++;
                    }
                }
            }
        }while(swap != 0);
    }
    private double distanceBetween(double[] point1, double[] point2)
    {
        double result = 0;
        for(int i = 0; i<point1.length; i++)
        {
            result += Math.pow((point1[i] - point2[i]),2);
        }
        return result;
    }
    private double[] getMeanVectorAfterAdd(double[] currentMean, double[] newPoint, int size)
    {
        double[] retV = new double[currentMean.length];
        for(int i = 0; i<retV.length; i++)
        {
            retV[i] = currentMean[i] * size + newPoint[i];
            retV[i] /= (size + 1);
        }
        return retV;
    }
    private double[] getMeanVectorAfterSub(double[] currentMean, double[] pointRemoved, int size)
    {
        double[] retV = new double[currentMean.length];
        for(int i = 0; i<retV.length; i++)
        {
            retV[i] = currentMean[i] * size - pointRemoved[i];
            retV[i] /= (size - 1);
        }
        return retV;
    }
    
    private int getMinIndex(double[] list)
    {
        int minIndex = -1;
        double min = Double.POSITIVE_INFINITY;
        for(int i = 0; i<list.length; i++)
        {
            if(list[i] < min)
            {
                min = list[i];
                minIndex = i;
            }
        }
        return minIndex;
    }
    
    private int getMaxIndex(double[] list)
    {
        int maxIndex = -1;
        double max = Double.NEGATIVE_INFINITY;
        for(int i = 0; i<list.length; i++)
        {
            if(list[i] > max)
            {
                max = list[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }
}
