/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kmeansclustering;

import java.awt.*;
import java.awt.event.*;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Siamul Karim Khan
 */
public class Graph extends Frame{
    int width;
    int height;
    public Graph()
    {
        super("K-means Clustering Results");
        prepareGUI();
    }
    
    private void prepareGUI(){
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        width = gd.getDisplayMode().getWidth() - 100;
        height = gd.getDisplayMode().getHeight() - 100;
        setSize(width, height);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }        
        }); 
    }    
    ArrayList<Integer>[] clusters;
    double[][] clusterMeans;
    double[][] dataSet;

    public void setClusters(ArrayList<Integer>[] clusters) {
        this.clusters = clusters;
    }

    public void setClusterMeans(double[][] clusterMeans) {
        this.clusterMeans = clusterMeans;
    }

    public void setDataSet(double[][] dataSet) {
        this.dataSet = dataSet;
    }
    ArrayList<Integer> R;
    ArrayList<Integer> G;
    ArrayList<Integer> B;
    public void set(double[][] dataSet, ArrayList<Integer>[] clusters, double[][] clusterMeans)
    {
        setDataSet(dataSet);
        setClusters(clusters);
        setClusterMeans(clusterMeans);
        int increment = (int)((double)255/(double)clusters.length);
        R = new ArrayList<>();
        G = new ArrayList<>();
        B = new ArrayList<>();
        for(int i = 0; i<255; i+=increment)
        {
            R.add(i);
            G.add(i);
            B.add(i);
        }
        Collections.shuffle(R);
        Collections.shuffle(G);
        Collections.shuffle(B);
    }
    @Override
    public void paint(Graphics g) {

        double[] max = findMax(dataSet);
        double[] min = findMin(dataSet);
        //System.out.println(max[0] + " " + max[1]);
        for(int i = 0; i<clusters.length; i++)
        {
            Color c = new Color(R.get(i), 255-G.get(i), B.get(i));
            g.setColor(c);
            for(Integer j : clusters[i])
            {
                int pointx = (int)(((dataSet[j][0]-min[0])/max[0])*(width - 60)) + 60;
                int pointy = (int)(((dataSet[j][1]-min[1])/max[1])*(height - 60)) + 60;
                g.drawOval(pointx, pointy, 10, 10);
            }
            int pointx = (int)(((clusterMeans[i][0]-min[0])/max[0])*(width - 60)) + 60;
            int pointy = (int)(((clusterMeans[i][1]-min[1])/max[1])*(height - 60)) + 60;
            g.drawLine(pointx - 10, pointy-1, pointx + 10, pointy-1);
            g.drawLine(pointx - 10, pointy, pointx + 10, pointy);
            g.drawLine(pointx - 10, pointy+1, pointx + 10, pointy+1);
            g.drawLine(pointx - 1, pointy - 10, pointx - 1, pointy + 10);
            g.drawLine(pointx, pointy - 10, pointx, pointy + 10);
            g.drawLine(pointx + 1, pointy - 10, pointx + 1, pointy + 10);
        }
    }
    private double[] findMax(double[][] dataSet)
    {
        double[] max = new double[2];
        max[0] = Double.NEGATIVE_INFINITY;
        max[1] = Double.NEGATIVE_INFINITY;
        for(int i = 0; i<dataSet.length; i++)
        {
            if(dataSet[i][0] > max[0])
            {
                max[0] = dataSet[i][0];
            }
            if(dataSet[i][1] > max[1])
            {
                max[1] = dataSet[i][1];
            }
        }
        return max;
    }
    private double[] findMin(double[][] dataSet)
    {
        double[] min = new double[2];
        min[0] = Double.POSITIVE_INFINITY;
        min[1] = Double.POSITIVE_INFINITY;
        for(int i = 0; i<dataSet.length; i++)
        {
            if(dataSet[i][0] < min[0])
            {
                min[0] = dataSet[i][0];
            }
            if(dataSet[i][1] < min[1])
            {
                min[1] = dataSet[i][1];
            }
        }
        return min;
    }
}
